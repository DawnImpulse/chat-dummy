/*
ISC License
Copyright 2018, Saksham
Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted,
provided that the above copyright notice and this permission notice appear in all copies.
THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
OR PERFORMANCE OF THIS SOFTWARE.*/
package com.dummy.chat

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var count = 49
    // create
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        linear.addView(layoutInflater.inflate(R.layout.inflator_names, null))
        addItemsInit()
        linear.setOnClickListener(this)
    }

    // click
    override fun onClick(v: View?) {
        v?.let {
            ++count
            addItem()
            scroll.post { scroll.fullScroll(View.FOCUS_DOWN) }
        }
    }

    // add items
    private fun addItemsInit() {
        var i = 0
        while (i < 50) {
            var layout = layoutInflater.inflate(R.layout.inflator_message, null)
            if (i % 2 == 0) {
                var count: Int = i / 2
                layout.findViewById<View>(R.id.send).visibility = View.VISIBLE
                (layout.findViewById<View>(R.id.sendText) as TextView).text = "Sender message no. $count"
            } else {
                var count: Int = i / 2
                layout.findViewById<View>(R.id.rec).visibility = View.VISIBLE
                (layout.findViewById<View>(R.id.recText) as TextView).text = "Receiver message no. $count"
            }
            linear.addView(layout)
            i++
        }
    }

    // add item
    private fun addItem() {
        var layout = layoutInflater.inflate(R.layout.inflator_message, null)
        if (count % 2 == 0) {
            var ct: Int = count / 2
            layout.findViewById<View>(R.id.send).visibility = View.VISIBLE
            (layout.findViewById<View>(R.id.sendText) as TextView).text = "Sender message no. $ct"
        } else {
            var ct: Int = count / 2
            layout.findViewById<View>(R.id.rec).visibility = View.VISIBLE
            (layout.findViewById<View>(R.id.recText) as TextView).text = "Receiver message no. $ct"
        }
        linear.addView(layout)
    }

    // navigate button
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

}
